#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import walk
from os.path import abspath, basename, dirname, join
import re
from shutil import rmtree
from tempfile import mkdtemp
from unittest import TestCase

from dpp.core import FileReader
from dpp.compiler import Compiler
from dpp.preprocessor import Preprocessor
from tests.utils import Utils

# test samples
sample_dir = abspath(join(dirname(__file__), '..', 'samples'))
def make_evaluator(folder):
    def evaluator(self):
        actual = self.target.compile(join(sample_dir, folder, 'Dockerfile.in'))
        with open(join(sample_dir, folder, 'Dockerfile'), 'r') as fd:
            expected = fd.read()
        self.assertDockerfilesEquivalent(expected, "\n".join(actual))
    evaluator.__name__ = "itest_" + basename(folder)
    return evaluator
samples = next(walk(sample_dir))[1]
sample_case_dict = {"target": Compiler(Preprocessor(FileReader()))}
for folder in samples:
    sample_case_dict['itest_' + basename(folder)] = make_evaluator(folder)

IntegrationTests = type('IntegrationTests', (TestCase, Utils),
                        sample_case_dict)

