#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase

from dpp.commands import Import

from tests.utils import FakeCompiler, Utils


class ImportTests(TestCase, Utils):
    def setUp(self):
        self.fake_compiler = FakeCompiler(self)

    def test_import(self):
        self.fake_compiler.compile_output = """
            FROM yesterday
            MAINTAINER nobody <nobody@example.com>
            RUN away
            CMD you
            LABEL me
            EXPOSE privates
            ENV iron ment
            ADD two numbers
            COPY someones homework
            ENTRYPOINT on concourse B
            VOLUME louder
            USER manual
            WORKDIR ectory
            ARG me hearties
            ONBUILD RUN a forklift
            ONBUILD CMD me
            STOPSIGNAL red
            HEALTHCHECK up
            SHELL game
        """.split("\n")
        self.assertCommandsProduce("""
            # IMPORT suppresses CMD, LABEL, EXPOSE, ENTRYPOINT, FROM
            #   and MAINTAINER
            RUN away
            ENV iron ment
            ADD two numbers
            COPY someones homework
            VOLUME louder
            USER manual
            WORKDIR ectory
            ARG me hearties
            STOPSIGNAL red
            HEALTHCHECK up
            SHELL game

            # IMPORT moves ONBUILD * to the end and strips off the ONBUILD so
            # it's just a regular command
            RUN a forklift

            # IMPORT should revert WORKDIR and SHELL
            SHELL ["/bin/sh", "-c"]
            WORKDIR /oldworkdir
        """, [Import('whatever', self.fake_compiler, {
            'workdir': '/oldworkdir',
            'shell': ['/bin/sh', '-c'],
        })])
