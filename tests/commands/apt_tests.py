#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase

from dpp.commands import Apt
from tests.utils import Utils

class AptTests(TestCase, Utils):
    def test_single_arg(self):
        self.assertCommandsProduce("""
            RUN apt-get -qq update --fix-missing \
             && apt-get -qqy -o=Dpkg::Use-Pty=0 install foo \
             && rm -rf /var/lib/apt/lists/*
        """, [Apt('foo', None, {})])

    def test_multi_arg(self):
        self.assertCommandsProduce("""
            RUN apt-get -qq update --fix-missing \
             && apt-get -qqy -o=Dpkg::Use-Pty=0 install foo bar baz \
             && rm -rf /var/lib/apt/lists/*
        """, [Apt('foo bar baz', None, {})])
