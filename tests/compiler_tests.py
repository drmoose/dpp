#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import walk
from os.path import abspath, basename, dirname, exists, isdir, join
import re
from shutil import rmtree
from tempfile import mkdtemp
from unittest import TestCase

from dpp.core import Command
from dpp.compiler import Compiler
from tests.utils import Utils


class FakePreprocessor(object):
    stream = []

    def preprocess(self, path):
        for item in self.stream:
            yield item, item


class CompilerTests(TestCase, Utils):
    def setUp(self):
        self.preprocessor = FakePreprocessor()
        self.registry = {}
        self.target = Compiler(self.preprocessor, self.registry)

    def assertCompilesTo(self, expected, stream):
        self.preprocessor.stream = stream
        self.assertDockerfilesEquivalent(
            expected,
            "\n".join(self.target.compile('nosetests/Dockerfile.in'))
        )

    def test_compile_handles_registered_commands(self):
        class Bleh(Command):
            def apply(self):
                yield ('VOLUME', ('/etc', self.args))
                yield ('RUN', 'echo "hello world"')
        self.registry['BLEH'] = Bleh
        self.assertCompilesTo(r"""
            VOLUME ["/etc", "cheese"]
            RUN echo "hello world"
        """, ('BlEh cheese',))

    def test_compile_passthrough(self):
        self.assertCompilesTo(r"""
            FOO bar\
                baz
            FEE ["fie", "foe"]
        """, (
            ('FOO bar baz'),
            ('FEE ["fie", "foe"]')
        ))

    def test_compile_writes_context_dockerfile(self):
        self.test_compile_passthrough()
        self.assert_(exists(join(self.target.root_dir, 'contexts',
                                 'nosetests', 'Dockerfile')))

    def test_compiler_folders(self):
        self.preprocessor.stream = ['MEH']
        for line in self.target.compile("/doe/ray/me"):
            if 'MEH' in line:
                break
        self.assertEqual("/doe/ray", self.target.source_dir)
        self.assert_(isdir(self.target.context_dir))
        self.assertEqual("ray", basename(self.target.context_dir))

    def test_compiler_subfolders(self):
        self.preprocessor.stream.extend([
            'SOURCE_FILE /foo/bar/baz',
            'BLEH',
            'END',
            'BLAH'
        ])
        itr = iter(self.target.compile("/doe/ray/me"))
        for x in itr:
            if x.startswith('BLEH'):
                break
        self.assertEqual("/foo/bar", self.target.source_dir)
        self.assert_(self.target.context_dir.endswith("ray/bar"),
                     self.target.context_dir)
        for x in itr:
            if x.startswith('BLAH'):
                break
        self.assertEqual("/doe/ray", self.target.source_dir)
        self.assert_(self.target.context_dir.endswith("ray"))

    def test_compiler_handles_with_block(self):
        class Bleh(Command):
            def apply(self):
                yield ('Applied %s' % self.args,)
            def remove(self):
                yield ('Removed %s' % self.args,)
        self.registry['BLEH'] = Bleh
        self.assertCompilesTo(r"""
            Applied kitty
            Pet kitty
            Removed kitty
        """, ('WITH BlEh kitty', 'Pet kitty', 'END'))
