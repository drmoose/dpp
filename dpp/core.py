#!/usr/bin/env python
# -*- coding: utf-8 -*-

from contextlib import contextmanager
from itertools import chain
from json import loads, dumps
from os import chdir, getcwd
from os.path import abspath, dirname, isdir, isfile, join
import re
from shutil import copy2, copytree


class Command(object):
    registry = {}
    opens_with_block = False

    @classmethod
    def register(self, cls_):
        name = cls_.name = getattr(cls_, 'name', cls_.__name__.upper())
        self.registry[name] = cls_
        return cls_

    def __init__(self, args, compiler, scope):
        self.args = args
        self.compiler = compiler
        self.scope = scope

    def apply(self):
        return []


class FileReader(object):
    @staticmethod
    def resolve_path(path):
        for child in ('Dockerfile.in', 'Dockerfile'):
            if isfile(join(path, child)):
                return join(path, child)
        return path

    def read(self, path):
        path = self.resolve_path(path)
        with open(path, 'r') as fd, pushd(dirname(abspath(path))):
            for line in fd:
                yield line


@contextmanager
def pushd(path):
    lastdir = getcwd()
    try:
        chdir(path)
        yield
    finally:
        chdir(lastdir)

