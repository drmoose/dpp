#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import chain
from os.path import basename
import stat
import re

from dpp.core import Command


@Command.register
class Script(Command):
    def __init__(self, *a, **kw):
        super(Script, self).__init__(*a, **kw)
        self.args = self.compiler.split_files(self.args)
        self.copies = []
        for optional, arg in enumerate(self.args):
            try:
                mode = stat.S_IRUSR if optional else stat.S_IRWXU
                self.copies.append(self.compiler.add_context(arg, mode=mode))
            except (OSError, IOError):
                if not optional:
                    raise

    def apply(self):
        yield ('COPY', " ".join(self.copies + [self.scope['workdir']]))
        yield ('RUN', ' && '.join([
            'cd %s' % self.scope['workdir'],
            './%s' % self.args[0] + ' ' + ' '.join(self.args[1:])
        ]))
