#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import environ, makedirs
from os.path import exists

try:
    import subprocess32 as subprocess
except ImportError:  # pragma: nocover
    import subprocess
from sys import stderr

from dpp.core import Command, pushd


SHELL_INVOKE = ['/bin/sh', '-o', 'xtrace', '-c']

@Command.register
class Prepare(Command):
    def apply(self):
        if not exists(self.compiler.context_dir):
            makedirs(self.compiler.context_dir)
        env = dict(environ)
        env['SOURCE'] = getattr(self.compiler, 'source_dir', '')
        with pushd(self.compiler.context_dir):
            subprocess.check_call(SHELL_INVOKE + [self.args],
                                  stdout=stderr, env=env)
