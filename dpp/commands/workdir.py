#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dpp.core import Command


@Command.register
class Workdir(Command):
    def apply(self):
        self.previous = self.scope['workdir']
        self.scope['workdir'] = self.args
        yield ("WORKDIR %s" % self.args,)

    def remove(self):
        self.scope['workdir'] = self.previous
        yield ("WORKDIR %s" % self.previous)
